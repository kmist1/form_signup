FORM_SIGNUP


### What is this repository for? ###

 Switch to develop branch to get the code. This is my first group assignment work which is SignUp form for bookface social web application.

### How do I get set up? ###

You need to install node.js to run this application after you download source code. Following are the steps to run the application in visual studio code
or webstorm.
step-1: put all the files in single folder name it client.
step-2: open same project in visual studio code or webstorm.
step-3: run the command "install i" to download all the dependencies.
step-4: run "npm start" command in terminal and you will be all set. 

Contact me here "https://kmist1.herokuapp.com/" if you are having problems running this application.
